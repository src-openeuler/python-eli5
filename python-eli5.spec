%global _empty_manifest_terminate_build 0
Name:		python-eli5
Version:	0.13.0
Release:	1
Summary:	Debug machine learning classifiers and explain their predictions
License:	MIT
URL:		https://github.com/eli5-org/eli5
Source0:	https://files.pythonhosted.org/packages/b5/30/3be87d2a7ca12a6ec45b8a5f6fd10be9992cc4b239dfb029c80091add477/eli5-0.13.0.tar.gz
BuildArch:	noarch


%description
ELI5 is a Python package which helps to debug machine learning classifiers and explain their predictions.

%package -n python3-eli5
Summary:	Debug machine learning classifiers and explain their predictions
Provides:	python-eli5
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-eli5
ELI5 is a Python package which helps to debug machine learning classifiers and explain their predictions.


%package help
Summary:	Development documents and examples for eli5
Provides:	python3-eli5-doc
%description help
ELI5 is a Python package which helps to debug machine learning classifiers and explain their predictions.


%prep
%autosetup -n eli5-0.13.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-eli5 -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Aug 03 2022 liukuo <liukuo@kylinos.cn> - 0.13.0-1
- Update to 0.13.0

* Thu May 27 2021 Python_Bot <Python_Bot@openeuler.org> - 0.11.0-1
- Package Spec generated

